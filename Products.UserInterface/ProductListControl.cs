﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Acme.Pharmacy.Products.Core.Entities;

namespace Products.UserInterface
{
    public partial class ProductListControl: UserControl
    {
        public ProductListControl()
        {
            InitializeComponent();
        }

        public void LoadData(IEnumerable<Product> products)
        {
            listView.Items.Clear();
            foreach (var product in products.OrderBy(p => p.PublicCode))
            {
                var listViewItem = new ListViewItem();
                SetListViewItem(listViewItem, product);
                listView.Items.Add(listViewItem);
            }
        }

        private void SetListViewItem(ListViewItem listViewItem, Product product)
        {
            listViewItem.SubItems.Clear();
            listViewItem.Text = product.PublicCode;
            listViewItem.SubItems.Add(product.Atc);
            listViewItem.SubItems.Add(product.ProductForm.Name);
            listViewItem.SubItems.Add(product.Packaging.Name);
            listViewItem.SubItems.Add(product.UnitsInPack.ToString());
            listViewItem.SubItems.Add(product.Price.ToString());
            listViewItem.Tag = product;
        }
    }
}
