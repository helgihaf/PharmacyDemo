﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Acme.Pharmacy.Common.Core.Services
{
    public interface IRepository<T>
    {
        void Insert(T entity);
        void Update(long id, T entity);
        void Delete(long id);
        IQueryable<T> SearchFor(Expression<Func<T, bool>> predicate);
        IQueryable<T> GetAll();
        T GetById(long id);
    }
}
