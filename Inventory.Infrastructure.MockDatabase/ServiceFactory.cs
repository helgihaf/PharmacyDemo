﻿using Acme.Pharmacy.Inventory.Core.Services;
using Moq;
using System.Linq;

namespace Acme.Pharmacy.Inventory.Infrastructure.MockDatabase
{
    public static class ServiceFactory
    {
        private static readonly Mock<IInventoryRepository> inventoryRepositoryMock = new Mock<IInventoryRepository>();

        static ServiceFactory()
        {
            var locations = new[]
            {
                new Core.Entities.Location { Id = 1, Name = "Aðal" },
                new Core.Entities.Location { Id = 2, Name = "Auka" },
            };

            var products = Acme.Pharmacy.Products.Intrastructure.MockDatabase.ServiceFactory.Products;
            var inventories = new[]
            {
                new Core.Entities.Inventory { Id = 1, Location = locations[0], Product = products[0], Count = 7 },
                new Core.Entities.Inventory { Id = 2, Location = locations[0], Product = products[1], Count = 16 },
                new Core.Entities.Inventory { Id = 3, Location = locations[0], Product = products[2], Count = 5 },
                new Core.Entities.Inventory { Id = 4, Location = locations[1], Product = products[0], Count = 5 },
                new Core.Entities.Inventory { Id = 5, Location = locations[1], Product = products[1], Count = 11 },
                new Core.Entities.Inventory { Id = 6, Location = locations[1], Product = products[2], Count = 23 },
            };
            inventoryRepositoryMock.Setup(p => p.GetAll()).Returns(inventories.AsQueryable());
        }

        public static IInventoryRepository InventoryRepository
        {
            get { return inventoryRepositoryMock.Object; }

        }
    }
}
