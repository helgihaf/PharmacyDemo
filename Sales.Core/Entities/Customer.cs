﻿using System.ComponentModel.DataAnnotations;

namespace Acme.Pharmacy.Sales.Core.Entities
{
    public class Customer
    {
        public long Id { get; set; }

        [Required]
        public string Name { get; set; }

    }
}