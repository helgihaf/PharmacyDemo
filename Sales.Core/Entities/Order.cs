﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Acme.Pharmacy.Sales.Core.Entities
{
    public class Order
    {
        public long Id { get; set; }

        [Required]
        public Customer Customer { get; set; }

        public DateTime Created { get; set; }

        public OrderStatus Status { get; set; }
        
        public IList<OrderLine> Lines { get; set; }
    }
}
