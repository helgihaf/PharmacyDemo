﻿using Acme.Pharmacy.Products.Core.Entities;
using System.ComponentModel.DataAnnotations;

namespace Acme.Pharmacy.Sales.Core.Entities
{
    public class OrderLine
    {
        public long Id { get; set; }

        [Required]
        public Order Order { get; set; }

        [Required]
        public Product Product { get; set; }

        public int OrderedCount { get; set; }

        public int DeliveredCount { get; set; }

        public int PendingCount
        {
            get { return OrderedCount - DeliveredCount; }
        }

        public decimal PendingPrice
        {
            get
            {
                return Product.Price * (1 + Product.TaxPercentage / 100) * PendingCount;
            }
        }

        public decimal PendingPriceWithoutTax
        {
            get
            {
                return Product.Price * PendingCount;
            }
        }
    }
}