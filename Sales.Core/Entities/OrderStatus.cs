﻿namespace Acme.Pharmacy.Sales.Core.Entities
{
    public enum OrderStatus
    {
        New,
        Active,
        Closed,
    }
}