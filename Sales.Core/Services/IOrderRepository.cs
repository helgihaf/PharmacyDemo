﻿using Acme.Pharmacy.Common.Core.Services;
using Acme.Pharmacy.Sales.Core.Entities;

namespace Acme.Pharmacy.Sales.Core.Services
{
    public interface IOrderRepository : IRepository<Order>
    {
    }
}
