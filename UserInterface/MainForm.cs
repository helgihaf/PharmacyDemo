﻿using System;
using System.Windows.Forms;

namespace Acme.Pharmacy.UserInterface
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            productsUserControl.CompositeService = new CompositeService();
        }

        private async void MainForm_Load(object sender, EventArgs e)
        {
            await productsUserControl.ShowData();
        }
    }
}
