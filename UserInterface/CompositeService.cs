﻿using Acme.Pharmacy.Products.Core.Services;
using Acme.Pharmacy.Sales.Core.Services;
using Acme.Pharmacy.Inventory.Core.Services;
using System.Linq;
using System.Threading.Tasks;

namespace Acme.Pharmacy.UserInterface
{
    public class CompositeService
    {
        private readonly IProductRepository productRepository;
        private readonly IInventoryRepository inventoryRepository;
        private readonly IOrderLineRepository orderLineRepository;

        public CompositeService()
        {
            productRepository = Products.Intrastructure.MockDatabase.ServiceFactory.ProductRepository;
            inventoryRepository = Inventory.Infrastructure.MockDatabase.ServiceFactory.InventoryRepository;
            orderLineRepository = Sales.Infrastructure.MockDatabase.ServiceFactory.OrderLineRepository;
        }

        public Task<IQueryable<ViewModel.Product>> GetProductsAsync()
        {
            return Task.Factory.StartNew(GetProducts);
        }


        public IQueryable<ViewModel.Product> GetProducts()
        {
            var groups =
                from product in productRepository.GetAll()
                join inventoryx in inventoryRepository.GetAll() on product.Id equals inventoryx.Product.Id into inventoryGroup
                join orderLinex in orderLineRepository.GetAll() on product.Id equals orderLinex.Product.Id into orderLineGroup
                from inventory in inventoryGroup.DefaultIfEmpty()
                from orderLine in orderLineGroup.DefaultIfEmpty()
                group new { product, inventory, orderLine } by product.Id into theGroup
                select theGroup;
            return
                from theGroup in groups
                select new ViewModel.Product
                {
                    Id = theGroup.First().product.Id,
                    PublicCode = theGroup.First().product.PublicCode,
                    Name = theGroup.First().product.Name,
                    Atc = theGroup.First().product.Atc,
                    UnitsInPack = theGroup.First().product.UnitsInPack,
                    Price = theGroup.First().product.Price,
                    ProductForm = theGroup.First().product.ProductForm.Name,
                    Packaging = theGroup.First().product.Packaging.Name,
                    TotalInventory = theGroup.Sum(g => g.inventory != null ? g.inventory.Count : 0),
                    TotalPendingSalesCount = theGroup.Sum(g => g.orderLine != null ? g.orderLine.PendingCount : 0),
                    TotalPendingSalesValue = theGroup.Sum(g => g.orderLine != null ? g.orderLine.PendingPrice : 0M)
                };
        }

    }
}
