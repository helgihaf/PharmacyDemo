﻿namespace Acme.Pharmacy.UserInterface.ViewModel
{
    public class Product
    {
        public long Id { get; set; }

        public string PublicCode { get; set; }

        public string Name { get; set; }

        public string Atc { get; set; }

        public int UnitsInPack { get; set; }

        public decimal Price { get; set; }

        public string ProductForm { get; set; }
        public string Packaging { get; set; }
        public int TotalInventory { get; set; }
        public int TotalPendingSalesCount { get; set; }
        public decimal TotalPendingSalesValue { get; set; }

    }
}
