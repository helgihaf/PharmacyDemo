﻿using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Acme.Pharmacy.UserInterface
{
    public partial class ProductsUserControl : UserControl
    {
        public ProductsUserControl()
        {
            InitializeComponent();
        }

        public CompositeService CompositeService { get; set; }

        public async Task ShowData()
        {
            var products = await CompositeService.GetProductsAsync();
            listView.SetObjects(products);
        }

    }
}
