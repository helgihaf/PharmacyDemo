﻿using Acme.Pharmacy.Products.Core.Entities;
using System;
using System.ComponentModel.DataAnnotations;

namespace Acme.Pharmacy.Inventory.Core.Entities
{
    public class InventoryTransaction
    {
        public long Id { get; set; }

        public DateTime DateTime { get; set; }

        [Required]
        public Product Product { get; set; }

        [Required]
        public Location Location { get; set; }

        public int Count { get; set; }

    }
}
