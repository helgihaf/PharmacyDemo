﻿using Acme.Pharmacy.Products.Core.Entities;
using System.ComponentModel.DataAnnotations;

namespace Acme.Pharmacy.Inventory.Core.Entities
{
    public class Inventory
    {
        public long Id { get; set; }

        [Required]
        public Product Product { get; set; }

        [Required]
        public Location Location { get; set; }

        public int Count { get; set; }

    }
}
