﻿using System.ComponentModel.DataAnnotations;

namespace Acme.Pharmacy.Inventory.Core.Entities
{
    public class Location
    {
        public long Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}