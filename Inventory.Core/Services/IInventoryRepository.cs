﻿using Acme.Pharmacy.Common.Core.Services;
using Acme.Pharmacy.Inventory.Core.Entities;

namespace Acme.Pharmacy.Inventory.Core.Services
{
    public interface IInventoryRepository : IRepository<Entities.Inventory>
    {
    }
}
