﻿using Acme.Pharmacy.Products.Core.Entities;
using Acme.Pharmacy.Inventory.Core.Entities;
using System;

namespace Acme.Pharmacy.Inventory.Core.Services
{
    /// <summary>
    /// Here is a service that is not a repository. Instead of having the <see cref="IInventoryRepository"/>
    /// and the <see cref="IInventoryTransactionRepository"/>, we could simply have the this service.
    /// We simply need to decide what level of control we want to offer our clients. If the <see cref="IInventoryControl"/>
    /// way were to be chosen, the <see cref="Entities.Inventory"/>  entity would probably not be needed.
    /// </summary>
    public interface IInventoryControl
    {
        /// <summary>
        /// Add <paramref name="count"/> number of items of <paramref name="product"/> to <paramref name="location"/>.
        /// </summary>
        /// <remarks>
        /// Note: It would probably be better an a real-world application to use the Id-s instead of the actual entities.
        /// </remarks>
        /// <param name="product"></param>
        /// <param name="location"></param>
        /// <param name="count"></param>
        void Add(Product product, Location location, int count);

        /// <summary>
        /// Remove <paramref name="count"/> number of items of <paramref name="product"/> from <paramref name="location"/>.
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="product"></param>
        /// <param name="location"></param>
        /// <param name="count"></param>
        void Remove(Product product, Location location, int count);

        /// <summary>
        /// Get the current inventory level of <paramref name="product"/> from <paramref name="location"/>.
        /// </summary>
        /// <param name="product"></param>
        /// <param name="location"></param>
        /// <returns></returns>
        int GetLevel(Product product, Location location);

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="product"></param>
        /// <param name="location"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        InventoryTransaction GetTransactions(Product product, Location location, DateTime from, DateTime to);
    }
}
