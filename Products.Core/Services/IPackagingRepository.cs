﻿using Acme.Pharmacy.Common.Core.Services;
using Acme.Pharmacy.Products.Core.Entities;

namespace Acme.Pharmacy.Products.Core.Services
{
    public interface IPackagingRepository : IRepository<Packaging>
    {
    }
}
