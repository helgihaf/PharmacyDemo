﻿using System.ComponentModel.DataAnnotations;

namespace Acme.Pharmacy.Products.Core.Entities
{
    public class ProductForm
    {
        public long Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}