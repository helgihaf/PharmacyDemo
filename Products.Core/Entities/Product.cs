﻿using System.ComponentModel.DataAnnotations;

namespace Acme.Pharmacy.Products.Core.Entities
{
    public class Product
    {
        public long Id { get; set; }

        [Required]
        public string PublicCode { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Atc { get; set; }

        public int UnitsInPack { get; set; }

        public decimal Price { get; set; }

        [Required]
        public ProductForm ProductForm { get; set; }

        [Required]
        public Packaging Packaging { get; set; }

        public decimal TaxPercentage { get; set; }
    }
}
