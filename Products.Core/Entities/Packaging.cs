﻿using System.ComponentModel.DataAnnotations;

namespace Acme.Pharmacy.Products.Core.Entities
{
    public class Packaging
    {
        public long Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}