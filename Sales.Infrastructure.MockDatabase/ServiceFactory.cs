﻿using Acme.Pharmacy.Sales.Core.Entities;
using Acme.Pharmacy.Sales.Core.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Acme.Pharmacy.Sales.Infrastructure.MockDatabase
{
    public static class ServiceFactory
    {
        private static readonly Mock<IOrderLineRepository> orderLineRepositoryMock = new Mock<IOrderLineRepository>();

        static ServiceFactory()
        {
            var customer = new Customer { Id = 1, Name = "Helgi" };
            var order = new Order { Id = 1, Created = DateTime.UtcNow, Customer = customer, Status = OrderStatus.Active, Lines = new List<OrderLine>() };
            var products = Acme.Pharmacy.Products.Intrastructure.MockDatabase.ServiceFactory.Products;

            var orderLine1 = new OrderLine
            {
                Id = 1,
                Order = order,
                Product = products[0],
                OrderedCount = 4,
                DeliveredCount = 0
            };
            var orderLines = new[]
            {
                
                new OrderLine { Id = 2, Order = order, Product = products[1], OrderedCount = 4, DeliveredCount = 3 },
            };
            orderLineRepositoryMock.Setup(p => p.GetAll()).Returns(orderLines.AsQueryable());
        }

        public static IOrderLineRepository OrderLineRepository
        {
            get { return orderLineRepositoryMock.Object; }

        }

    }
}
