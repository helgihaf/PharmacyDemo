﻿using Acme.Pharmacy.Products.Core.Entities;
using Acme.Pharmacy.Products.Core.Services;
using Moq;
using System.Linq;

namespace Acme.Pharmacy.Products.Intrastructure.MockDatabase
{
    public static class ServiceFactory
    {
        private static readonly Mock<IProductRepository> productRepositoryMock = new Mock<IProductRepository>();

        public static readonly ProductForm[] ProductForms = new[]
        {

            new ProductForm { Id = 1, Name = "Munndreifitafla" },
            new ProductForm { Id = 2, Name = "Filmuhúðuð tafla" },
            new ProductForm { Id = 3, Name = "Stungulyf, lausn" },
        };

        public static readonly Packaging[] Packaging = new[]
        {
            new Packaging { Id = 1, Name = "Þynnupakkning" },
            new Packaging { Id = 2, Name = "Lykja" },
        };

        public static readonly Product[] Products;

        static ServiceFactory()
        {
            Products = new[]
            {
                new Product { Id = 1, PublicCode="010953", Name = "Abilify", Atc="N05AX1", UnitsInPack=28, Price = 23359, TaxPercentage=24,
                    ProductForm = ProductForms.Single(f => f.Name == "Munndreifitafla"),
                    Packaging = Packaging.Single(p => p.Name == "Þynnupakkning"),
                },
                new Product { Id = 2, PublicCode="523151", Name = "Quetiapin Actavis", Atc="N05AH04", UnitsInPack=100, Price = 3903, TaxPercentage=24,
                    ProductForm = ProductForms.Single(f => f.Name == "Filmuhúðuð tafla"),
                    Packaging = Packaging.Single(p => p.Name == "Þynnupakkning"),
                },
                new Product { Id = 3, PublicCode="015929", Name = "Morphine Sulphate BP", Atc="N02AA01", UnitsInPack=10, Price = 2848, TaxPercentage=11,
                    ProductForm = ProductForms.Single(f => f.Name == "Stungulyf, lausn"),
                    Packaging = Packaging.Single(p => p.Name == "Lykja"),
                }
            };
            productRepositoryMock.Setup(p => p.GetAll()).Returns(Products.AsQueryable());
        }

        public static IProductRepository ProductRepository
        {
            get { return productRepositoryMock.Object; }

        }
    }
}
